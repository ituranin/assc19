/*
 * global.c
 *
 * Created: 18.05.2019 18:04:54
 *  Author: driverless
 */ 

#include "global.h"

AsscInfo info = {
	.state = AS_STATE_OFF,
	.substate = WAIT_MISSION,
	.errorBits = 0,
	.resBoxRunning = false,
	.resState = 0,
	.goState = false,
	.assiMode = ASSI_MODE_PARTY,
	.assiColor = ASSI_COLOR_YELLOW,
	.assiRulesState = ASSI_STATE_OFF,
	.buzzerMode = 0,
	.buzzerActive = false,
	.vehicleSpeed = 0,
	.rpm = 0,
	.gear = 0,
	.mission = 0,
	.asmsOn = false,
	.shutDownCircuitClosed = false,
	.triggeredShutdownCircuit = false,
	.timeoutsSet = false,
	.scStatus0 = false,
	.scStatus1 = false,
	.scStatus2 = false,
	.camera = true,
	.lidar = true,
	.imu = true,
	.disableChecks = false,
	.tsIsOn = false,
	.tsIsOnCounter = 0,
	.assiCounter = 0,
	.assiCheckCounter = 0,
	.yellowValueMid = 0,
	.blueValueMid = 0,
	.shouldCheckAssi = false,
	.assiCurrentToCheck = ASSI_CURRENT_THRESHOLD_FLASHING,
	.assiError = false
};

AsscSensor sensorHolder = {};

TimoutCounter timeoutsAssc = {
	.sb = STANDARD_TIMEOUT_MS,
	.sac = STANDARD_TIMEOUT_MS,
	.cac = STANDARD_TIMEOUT_MS,
	.etc = STANDARD_TIMEOUT_MS,
	.hsc = STANDARD_TIMEOUT_MS,
	.ebs = STANDARD_TIMEOUT_MS,
	.ebsSubstate = STANDARD_TIMEOUT_MS
};

asStates subSystemStates = {};
asStates subSystemStatesReceive = {};
	
SysLogData syslogData = {};

AsscErrorMasks errorMasks = {};
canMessageBuilder* cmb = NULL;
	
ErrorStates errorStates = {
	.assc = false,
	.ascpu = false,
	.vc = false,
	.ebs = false,
	.sac = false,
	.cac = false,
	.swc = false,
	.etc = false,
	.hsc = false,
	.ami = false,
	.mcu = false,
	.sb = false
};

struct can_message resStartupMsg = {};
uint8_t res_array[8];

void initResMessage()
{
	resStartupMsg.id = 0x000;
	resStartupMsg.len = 8;
	resStartupMsg.data = res_array;

	resStartupMsg.data[0] = 1;
	resStartupMsg.data[1] = 0;
	resStartupMsg.data[2] = 0;
	resStartupMsg.data[3] = 0;
	resStartupMsg.data[4] = 0;
	resStartupMsg.data[5] = 0;
	resStartupMsg.data[6] = 0;
	resStartupMsg.data[7] = 0;
}

/*
 * first loads error masks, than makes them available for calibration
 */
void initErrorMasks()
{
	// load
	loadErrorBitMask(&errorMasks.assc, DEV_ID_ASSC);
	loadErrorBitMask(&errorMasks.ascpu, DEV_ID_ASCPU);
	loadErrorBitMask(&errorMasks.vc, DEV_ID_ASGPU);
	loadErrorBitMask(&errorMasks.ebs, DEV_ID_ASEBS);
	loadErrorBitMask(&errorMasks.sac, DEV_ID_ASSAC);
	loadErrorBitMask(&errorMasks.cac, DEV_ID_ASCAC);
	loadErrorBitMask(&errorMasks.swc, DEV_ID_ASSWC);
	loadErrorBitMask(&errorMasks.etc, DEV_ID_ASETC);
	loadErrorBitMask(&errorMasks.hsc, DEV_ID_ASHSC);
	loadErrorBitMask(&errorMasks.ami, DEV_ID_AMI);
	loadErrorBitMask(&errorMasks.mcu, DEV_ID_MCU);
	loadErrorBitMask(&errorMasks.sb, DEV_ID_ASSB);
	
	// add for calibration
	addCANCalibrationBitMasks(cmb, &errorMasks.assc, DEV_ID_ASSC);
	addCANCalibrationBitMasks(cmb, &errorMasks.ascpu, DEV_ID_ASCPU);
	addCANCalibrationBitMasks(cmb, &errorMasks.vc, DEV_ID_ASGPU);
	addCANCalibrationBitMasks(cmb, &errorMasks.ebs, DEV_ID_ASEBS);
	addCANCalibrationBitMasks(cmb, &errorMasks.sac, DEV_ID_ASSAC);
	addCANCalibrationBitMasks(cmb, &errorMasks.cac, DEV_ID_ASCAC);
	addCANCalibrationBitMasks(cmb, &errorMasks.swc, DEV_ID_ASSWC);
	addCANCalibrationBitMasks(cmb, &errorMasks.etc, DEV_ID_ASETC);
	addCANCalibrationBitMasks(cmb, &errorMasks.hsc, DEV_ID_ASHSC);
	addCANCalibrationBitMasks(cmb, &errorMasks.ami, DEV_ID_AMI);
	addCANCalibrationBitMasks(cmb, &errorMasks.mcu, DEV_ID_MCU);
	addCANCalibrationBitMasks(cmb, &errorMasks.sb, DEV_ID_ASSB);
}

uint16_t to16bit(uint8_t low, uint8_t high)
{
	return low | (high << 8);
}

bool tsIsOff()
{
	if(info.rpm >= 3500)
	{
		return false;
	}
	
	if(info.rpm < 1000)
	{
		return true;
	}
	
	return true;
}

bool r2dIsOff()
{
	if(info.gear == 0)
	{
		return true;
	}
	
	return false;
}

bool asMissionSelected()
{
	if (info.mission > 2)
	{
		return true;
	}
	
	return false;
}

bool manualMissionSelected()
{
	if (info.mission == 2)
	{
		return true;
	}
	
	return false;
}

inline bool stateIsOk(uint8_t should, uint8_t is, int *counter)
{
	if (getCSState(should) != getCSState(is))
	{
		if (*counter <= 0)
		{
			return false;
		}
	}
	
	return true;
}

inline bool substateIsOk(uint8_t should, uint8_t is, int *counter)
{
	if (getCSSubstate(should) != getCSSubstate(is))
	{
		if (*counter <= 0)
		{
			return false;
		}
	}
	
	return true;
}

// returns true when error accures
bool checkErrorMasks()
{
	errorStates.sb = checkSyslogForErrors(syslogData.sb, &errorMasks.sb);
	errorStates.sac = checkSyslogForErrors(syslogData.sac, &errorMasks.sac);
	errorStates.cac = checkSyslogForErrors(syslogData.cac, &errorMasks.cac);
	errorStates.etc = checkSyslogForErrors(syslogData.etc, &errorMasks.etc);
	errorStates.hsc = checkSyslogForErrors(syslogData.hsc, &errorMasks.hsc);
	errorStates.ascpu = checkSyslogForErrors(syslogData.ascpu, &errorMasks.ascpu);
	errorStates.ami = checkSyslogForErrors(syslogData.ami, &errorMasks.ami);
	errorStates.ebs = checkSyslogForErrors(syslogData.ebs, &errorMasks.ebs);
	
	return errorStates.sb
			|| errorStates.sac
			|| errorStates.cac
			|| errorStates.etc
			|| errorStates.hsc
			|| errorStates.ascpu
			|| errorStates.ami
			|| errorStates.ebs;
}

void checkSubSystemStates()
{
	bool masksCheck = false;
	//bool masksCheck = checkErrorMasks();
	
	// TODO add dead end state which shows restart required in every state that needs it
	if (info.state == AS_STATE_OFF || info.state == AS_STATE_MANUAL || info.state == AS_STATE_FINISHED)
	{
		return;
	}
	
	if (stateIsOk(subSystemStates.sbState, subSystemStatesReceive.sbState, &timeoutsAssc.sb)
		&& stateIsOk(subSystemStates.sacState, subSystemStatesReceive.sacState, &timeoutsAssc.sac)
		&& stateIsOk(subSystemStates.cacState, subSystemStatesReceive.cacState, &timeoutsAssc.cac)
		&& stateIsOk(subSystemStates.etcState, subSystemStatesReceive.etcState, &timeoutsAssc.etc)
		&& stateIsOk(subSystemStates.hscState, subSystemStatesReceive.hscState, &timeoutsAssc.hsc)
		&& stateIsOk(subSystemStates.ebsState, subSystemStatesReceive.ebsState, &timeoutsAssc.ebs)
		//&& substateIsOk(subSystemStates.ebsState, subSystemStatesReceive.ebsState, &timeoutsAssc.ebsSubstate)
		&& !masksCheck
		&& info.camera
		&& info.lidar
		&& info.imu
		)
	{
		return;
	}
	
	info.state = AS_STATE_EBS;
	info.substate = 2;
}

void checkResAsmsAssi()
{
	if (info.state == AS_STATE_OFF || info.state == AS_STATE_MANUAL)
	{
		return;
	}
	
	if ((!info.asmsOn && info.state != AS_STATE_FINISHED) || (!info.shutDownCircuitClosed && getCSState(subSystemStatesReceive.cpuState) != CPU_STATE_FINISHED))
	{
		info.state = AS_STATE_EBS;
		info.substate = 3;
	}
	
	if (info.state == AS_STATE_FINISHED)
	{
		return;
	}
	
	if (info.assiError)
	{
		info.state = AS_STATE_EBS;
		info.substate = 5;
	}
}

void checkEBSsensorBits()
{
	if (syslogData.ebs[1] != 0 
		|| syslogData.ebs[2] != 0
		|| syslogData.ebs[5] != 0 
		|| syslogData.sb[1] != 0
		|| syslogData.sb[2] != 0
		|| syslogData.hsc[1] != 0
		//|| syslogData.hsc[2] != 0
		|| syslogData.hsc[4] != 0)
	{
		gpio_set_pin_level(EBS_FAILURE_LIGHT, true);
		
		if (info.state == AS_STATE_OFF || info.state == AS_STATE_MANUAL || info.state == AS_STATE_FINISHED)
		{
			return;
		}
		
		info.state = AS_STATE_EBS;
		info.substate = 4;
	}
	else
	{
		gpio_set_pin_level(EBS_FAILURE_LIGHT, false);
	}
}