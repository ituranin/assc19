/*
 * config.h
 *
 * Created: 18.05.2019 17:29:11
 *  Author: driverless
 */ 

#ifndef CONFIG_H_

#define CONFIG_H_



//////////////////////////////////////////////////////////////////////////

#define DEVICE_ID DEV_ID_ASSC			//define DEVICE ID - see header file dev ids

//////////////////////////////////////////////////////////////////////////

#define ASSI_MODE_STEADY   0
#define ASSI_MODE_FLASHING 1
#define ASSI_MODE_PARTY    2

#define ASSI_COLOR_OFF     0
#define ASSI_COLOR_YELLOW  1
#define ASSI_COLOR_BLUE    2

#define ASSI_BUZZER_OFF    0
#define ASSI_BUZZER_ON     1

#define AVERAGING_BUFFER_SIZE_POTENCY 5

//////////////////////////////////////////////////////////////////////////

//Sensor standard configs

//#define SENSOR_X_GRADIENT 3413

//#define SENSOR_X_SHIFT      10

//#define SENSOR_X_OFFSET      0

#endif /* CONFIG_H_ */

