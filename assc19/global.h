/*
 * global.h
 *
 * Created: 18.05.2019 17:47:57
 *  Author: driverless
 */ 


#ifndef GLOBAL_H_
#define GLOBAL_H_

#include "config.h"
#include "atmel_start.h"
#include "armcustomdrivers/config/asStates.h"
#include "armcustomdrivers/stateLEDInterface/stateLED.h"
#include "armcustomdrivers/sensorInterface/sensors.h"
#include "armcustomdrivers/utilities/errorBitMasks.h"
#include "armcustomdrivers/config/deviceIDs.h"
#include "armcustomdrivers/canInterface/canMsgBuilder.h"
#include "states/states.h"

#define STATE_COUNT 7
#define MSG_ID_RES 0x191
#define MSG_ID_DASH 0x201
#define MESSAGE_FILTER 0xFF0
#define DEVICE_ID_FILTER 0x00F
#define STANDARD_TIMEOUT_MS 600
#define SC_CLOSED 7
#define ASSI_CURRENT_THRESHOLD_STEADY 350
#define ASSI_CURRENT_THRESHOLD_FLASHING 175

typedef struct
{
	uint8_t state;
	uint8_t substate;
	uint8_t errorBits;
	bool resBoxRunning;
	int resState;
	bool goState;
	int assiMode;
	int assiColor;
	int buzzerMode;
	bool buzzerActive;
	uint8_t assiRulesState;
	uint16_t vehicleSpeed;
	uint16_t rpm;
	uint8_t gear;
	uint8_t mission;
	bool asmsOn;
	bool shutDownCircuitClosed;
	bool triggeredShutdownCircuit;
	bool timeoutsSet;
	bool scStatus0;
	bool scStatus1;
	bool scStatus2;
	bool camera;
	bool lidar;
	bool imu;
	bool disableChecks;
	bool tsIsOn;
	int tsIsOnCounter;
	int assiCounter;
	int assiCheckCounter;
	int yellowValueMid;
	int blueValueMid;
	bool shouldCheckAssi;
	int assiCurrentToCheck;
	bool assiError;
} AsscInfo;

typedef struct  
{
	sensor* sensor1Diag; // Sensor-Versorgungs-Spannung
	sensor* sensor2Diag; // Sensor-Versorgungs-Spannung
	sensor* scIRDiag;
	sensor* scEBSRDiag;
	sensor* blueASSIDiag;
	sensor* ebsDiag;
	sensor* aDiag; // System-Batterie-Spannung
	sensor* cDiag; // System-Batterie-Spannung
	sensor* scFPRDiag;
	sensor* yellowASSIDiag;
} AsscSensor;

typedef struct {
	errorBitMasks assc;
	errorBitMasks ascpu;
	errorBitMasks vc;
	errorBitMasks ebs;
	errorBitMasks sac;
	errorBitMasks cac;
	errorBitMasks swc;
	errorBitMasks etc;
	errorBitMasks hsc;
	errorBitMasks ami;
	errorBitMasks mcu;
	errorBitMasks sb;
} AsscErrorMasks;

typedef struct {
	uint8_t assc[8];
	uint8_t ascpu[8];
	uint8_t vc[8];
	uint8_t ebs[8];
	uint8_t sac[8];
	uint8_t cac[8];
	uint8_t swc[8];
	uint8_t etc[8];
	uint8_t hsc[8];
	uint8_t ami[8];
	uint8_t mcu[8];
	uint8_t sb[8];
} SysLogData;

typedef struct {
	bool assc;
	bool ascpu;
	bool vc;
	bool ebs;
	bool sac;
	bool cac;
	bool swc;
	bool etc;
	bool hsc;
	bool ami;
	bool mcu;
	bool sb;
} ErrorStates;

typedef struct 
{
	int sb;
	int sac;
	int cac;
	int etc;
	int hsc;
	int ebs;
	int ebsSubstate;
} TimoutCounter;

extern canMessageBuilder* cmb;
extern AsscInfo info;
extern AsscSensor sensorHolder;
extern TimoutCounter timeoutsAssc;
extern asStates subSystemStates;
extern asStates subSystemStatesReceive;
extern AsscErrorMasks errorMasks;
extern SysLogData syslogData;
extern ErrorStates errorStates;
extern struct can_message resStartupMsg;

void initResMessage();
void initErrorMasks();
uint16_t to16bit(uint8_t low, uint8_t high);

bool r2dIsOff();
bool tsIsOff();
bool asMissionSelected();
bool manualMissionSelected();

// triggers EBS if Sum Ting Wong
bool checkErrorMasks();
void checkSubSystemStates();
void checkResAsmsAssi();
void checkEBSsensorBits();

#endif /* GLOBAL_H_ */