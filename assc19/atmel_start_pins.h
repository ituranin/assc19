/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */
#ifndef ATMEL_START_PINS_H_INCLUDED
#define ATMEL_START_PINS_H_INCLUDED

#include <hal_gpio.h>

// SAMC21 has 9 pin functions

#define GPIO_PIN_FUNCTION_A 0
#define GPIO_PIN_FUNCTION_B 1
#define GPIO_PIN_FUNCTION_C 2
#define GPIO_PIN_FUNCTION_D 3
#define GPIO_PIN_FUNCTION_E 4
#define GPIO_PIN_FUNCTION_F 5
#define GPIO_PIN_FUNCTION_G 6
#define GPIO_PIN_FUNCTION_H 7
#define GPIO_PIN_FUNCTION_I 8

#define VCC_ACT_DIAG GPIO(GPIO_PORTA, 2)
#define VCC_SYSTEM_DIAG GPIO(GPIO_PORTA, 3)
#define SENSOR_2_DIAG GPIO(GPIO_PORTA, 4)
#define SENSOR_1_DIAG GPIO(GPIO_PORTA, 5)
#define SC_FP_REL_DIAG GPIO(GPIO_PORTA, 6)
#define ASSI_YELLOW_DIAG GPIO(GPIO_PORTA, 7)
#define SC_IGN_REL_DIAG GPIO(GPIO_PORTA, 8)
#define ASSI_BLUE_DIAG GPIO(GPIO_PORTA, 9)
#define VCC_EBS_DIAG GPIO(GPIO_PORTA, 10)
#define SC_EBS_REL_DIAG GPIO(GPIO_PORTA, 11)
#define WD_OUTPUT GPIO(GPIO_PORTA, 14)
#define ACTIVATE_EBS GPIO(GPIO_PORTA, 15)
#define EN_ASSI_YELLOW GPIO(GPIO_PORTA, 16)
#define EN_ASSI_BLUE GPIO(GPIO_PORTA, 17)
#define EBS_FAILURE_LIGHT GPIO(GPIO_PORTA, 18)
#define ASMS_IS_ACTIVE GPIO(GPIO_PORTA, 19)
#define SC_STATUS_2 GPIO(GPIO_PORTA, 22)
#define SC_STATUS_1 GPIO(GPIO_PORTA, 23)
#define CAN_TX GPIO(GPIO_PORTA, 24)
#define CAN_RX GPIO(GPIO_PORTA, 25)
#define SC_STATUS_0 GPIO(GPIO_PORTA, 27)

#endif // ATMEL_START_PINS_H_INCLUDED
