/*
 * ready.c
 *
 * Created: 18.05.2019 17:52:21
 *  Author: driverless
 */ 

#include "states.h"

static struct timer_task wait5SecondsTimer;
bool waitTimerStarted = false;

static void wait5SecondsTimerTask(const struct timer_task *const timer_task)
{
	info.substate = WAIT_FOR_GO;
	info.timeoutsSet = false;
}

void wait5Seconds()
{
	if(!waitTimerStarted)
	{
		wait5SecondsTimer.interval = 5000;
		wait5SecondsTimer.mode = TIMER_TASK_ONE_SHOT;
		wait5SecondsTimer.cb = wait5SecondsTimerTask;
		timer_add_task(&TIMER_0, &wait5SecondsTimer);
		waitTimerStarted = true;
	}
}

void waitForGo()
{
	if (info.goState)
	{
		info.substate = WAIT_FOR_DRIVING;
		info.timeoutsSet = false;
	}
}

void waitForDriving()
{
	subSystemStates.sbState = getFullCSState(SB_STATE_AVAILABLE, 0);
	subSystemStates.cacState = getFullCSState(CA_STATE_AVAILABLE, 0);
	
	if (!info.timeoutsSet)
	{
		timeoutsAssc.sb = STANDARD_TIMEOUT_MS;
		timeoutsAssc.cac = STANDARD_TIMEOUT_MS;
		info.timeoutsSet = true;
	}
	
	if ((getCSState(subSystemStates.sbState) == getCSState(subSystemStatesReceive.sbState))
		&& (getCSState(subSystemStates.cacState) == getCSState(subSystemStatesReceive.cacState)))
		//&& !r2dIsOff())
	{
		info.state = AS_STATE_DRIVING;
		info.substate = WAIT_FOR_MISSION_FINISHED;
		info.timeoutsSet = false;
	}
}

void ready()
{
	setStateLED(CYAN);
	
	info.assiColor = ASSI_COLOR_YELLOW;
	info.assiMode = ASSI_MODE_STEADY;
	info.buzzerMode = ASSI_BUZZER_OFF;
	info.assiRulesState = ASSI_STATE_YELLOW_CONST;
	
	switch (info.substate)
	{
		case WAIT_5_SECONDS: wait5Seconds(); break;
		case WAIT_FOR_GO: waitForGo(); break;
		case WAIT_FOR_DRIVING: waitForDriving(); break;
	}
}