/*
 * manual.c
 *
 * Created: 18.05.2019 17:53:32
 *  Author: driverless
 */ 

#include "states.h"

void manual()
{
	setStateLED(YELLOW);
	
	info.assiColor = ASSI_COLOR_OFF;
	info.assiMode = ASSI_MODE_STEADY;
	info.buzzerMode = ASSI_BUZZER_OFF;
	info.assiRulesState = ASSI_STATE_OFF;
	
	if(!info.shutDownCircuitClosed)
	{
		subSystemStates.amiState = getFullCSState(AMI_STATE_SHOW_RESTART, AMI_SUBSTATE_CLOSE_SC);
	} else {
		subSystemStates.amiState = getFullCSState(AMI_STATE_LOCKED, AMI_SUBSTATE_NORMAL);
	}
}