/*
 * light.c
 *
 * Created: 18.05.2019 17:53:47
 *  Author: driverless
 */ 

#include "states.h"

bool msgSend = false;
uint8_t msgId = 0x1A0;

static struct timer_task ledTimer;
bool ledTimerInitialized = false;

static void ledTimerTask(const struct timer_task *const timer_task)
{
	if(msgId > 0x1AE)
	{
		//msgSend = true;
		msgId = 0x1A0;
	} else {
		msgId++;
	}
	
	if(!msgSend)
	{
		struct can_message msg;
		msg.id = msgId;
		msg.len = 8;
		msg.data[0] = 6;
		can_async_write(&CAN_0, &msg);
	}
}

void light()
{
	setStateLED(WHITE);
	
	info.disableChecks = true;
	info.assiColor = ASSI_COLOR_OFF;
	info.assiMode = ASSI_MODE_STEADY;
	info.buzzerMode = ASSI_BUZZER_OFF;
	subSystemStates.amiState = getFullCSState(AMI_STATE_SHOW_RESTART, 0);
	
	/*if (!ledTimerInitialized)
	{
		ledTimerInitialized = true;
		
		ledTimer.interval = 250;
		ledTimer.mode = TIMER_TASK_REPEAT;
		ledTimer.cb = ledTimerTask;
		
		timer_add_task(&TIMER_0, &ledTimer);
	}*/
}