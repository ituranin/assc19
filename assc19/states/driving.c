/*
 * driving.c
 *
 * Created: 18.05.2019 17:52:37
 *  Author: driverless
 */ 

#include "states.h"

void waitForMissionFinished()
{
	if ((getCSState(subSystemStatesReceive.cpuState) == CPU_STATE_FINISHED)
		&& info.vehicleSpeed == 0)
	{
		info.substate = WAIT_FOR_FINISHED;
		info.timeoutsSet = false;
	}
}

void waitForFinished()
{
	subSystemStates.hscState = getFullCSState(HS_STATE_AVAILABLE, 0);
	subSystemStates.ebsState = getFullCSState(EBS_STATE_ACTIVATED, 0);
	subSystemStates.cacState = getFullCSState(CA_STATE_ENGAGED, 0);
	subSystemStates.etcState = getFullCSState(ETC_STATE_OFF, 0);
	
	if (!info.timeoutsSet)
	{
		timeoutsAssc.hsc = STANDARD_TIMEOUT_MS;
		timeoutsAssc.cac = STANDARD_TIMEOUT_MS;
		timeoutsAssc.etc = STANDARD_TIMEOUT_MS;
		timeoutsAssc.ebs = STANDARD_TIMEOUT_MS;
		info.timeoutsSet = true;
	}
	
	if ((getCSState(subSystemStates.hscState) == getCSState(subSystemStatesReceive.hscState))
		&& (getCSState(subSystemStates.ebsState) == getCSState(subSystemStatesReceive.ebsState))
		&& (getCSState(subSystemStates.cacState) == getCSState(subSystemStatesReceive.cacState))
		&& (getCSState(subSystemStates.etcState) == getCSState(subSystemStatesReceive.etcState))
		//&& r2dIsOff()
		&& tsIsOff())
	{
		info.state = AS_STATE_FINISHED;
		info.substate = 0;
		info.timeoutsSet = false;
	}
}

void driving()
{
	setStateLED(GREEN);
	
	info.assiColor = ASSI_COLOR_YELLOW;
	info.assiMode = ASSI_MODE_FLASHING;
	info.buzzerMode = ASSI_BUZZER_OFF;
	info.assiRulesState = ASSI_STATE_YELLOW_FLASH;
	
	switch(info.substate)
	{
		case WAIT_FOR_MISSION_FINISHED: waitForMissionFinished(); break;
		case WAIT_FOR_FINISHED: waitForFinished(); break;
	}
}