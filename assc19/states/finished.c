/*
 * finished.c
 *
 * Created: 18.05.2019 17:52:54
 *  Author: driverless
 */ 

#include "states.h"

void finished()
{
	setStateLED(BLUE);
	
	info.assiColor = ASSI_COLOR_BLUE;
	info.assiMode = ASSI_MODE_STEADY;
	info.buzzerMode = ASSI_BUZZER_OFF;
	info.assiRulesState = ASSI_STATE_BLUE_CONST;
	subSystemStates.amiState = getFullCSState(AMI_STATE_SHOW_RESTART, AMI_SUBSTATE_NORMAL);
}