/*
 * states.h
 *
 * Created: 18.05.2019 17:45:14
 *  Author: driverless
 */ 


#ifndef STATES_H_
#define STATES_H_

#include "global.h"
#include "armcustomdrivers/stateLEDInterface/stateLED.h"

enum OffSubstate
{
	WAIT_MISSION = 1,
	AS_WAIT_ASMS,
	AS_CHECK_BRAKE_PRESSURE_LOW,
	AS_TURN_ON_PUMP,
	AS_WAIT_EBS_ETC_CAC,
	AS_WAIT_BRAKE_ENGAGED,
	AS_WAIT_TRACTIVE,
	AS_WAIT_FOR_READY,
	MANUAL_WAIT_TRACTIVE,
	MANUAL_WAIT_FOR_MANUAL,
	DEAD_END
};

enum ReadySubstate
{
	WAIT_5_SECONDS = 16,
	WAIT_FOR_GO,
	WAIT_FOR_DRIVING
};

enum DrivingSubstate
{
	WAIT_FOR_MISSION_FINISHED = 20,
	WAIT_FOR_FINISHED
};

void off();
void ready();
void driving();
void finished();
void ebs();
void manual();
void light();

#endif /* STATES_H_ */