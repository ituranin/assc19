/*
 * off.c
 *
 * Created: 18.05.2019 17:50:53
 *  Author: driverless
 */ 

#include "states.h"

// TODO in Timeouts soll-states �ndern

inline void deadEnd()
{
	subSystemStates.amiState = getFullCSState(AMI_STATE_SHOW_RESTART, AMI_SUBSTATE_NORMAL);
	subSystemStates.etcState = getFullCSState(ETC_STATE_OFF, 0);
	subSystemStates.hscState = getFullCSState(HS_STATE_UNAVAILABLE, 0);
}

inline void waitMission()
{	
	uint8_t mission = getCSSubstate(subSystemStatesReceive.amiState);
	if (mission > 2)
	{
		info.substate = AS_WAIT_ASMS;
	}
	
	if (mission == 1)
	{
		info.substate = MANUAL_WAIT_TRACTIVE;
	}
	
	/*if (mission == MISSION_MAINTENANCE)
	{
		info.disableChecks = true;
		info.state = AS_STATE_LIGHT;
		info.substate = 0;
	}*/
	
	subSystemStates.etcState = getFullCSState(ETC_STATE_OFF, 0);
}

inline void manualWaitTractive()
{
	subSystemStates.ebsState = getFullCSState(EBS_STATE_UNAVAILABLE, 0);
	subSystemStates.hscState = getFullCSState(HS_STATE_UNAVAILABLE, 0);
	subSystemStates.etcState = getFullCSState(ETC_STATE_MANUAL, 0);
	
	if (!info.timeoutsSet)
	{
		timeoutsAssc.ebs = STANDARD_TIMEOUT_MS;
		timeoutsAssc.hsc = STANDARD_TIMEOUT_MS;
		timeoutsAssc.etc = STANDARD_TIMEOUT_MS;
		info.timeoutsSet = true;
	}
	
	if ((getCSState(subSystemStates.ebsState) == getCSState(subSystemStatesReceive.ebsState))
		&& (getCSState(subSystemStates.hscState) == getCSState(subSystemStatesReceive.hscState))
		&& (getCSState(subSystemStates.etcState) == getCSState(subSystemStatesReceive.etcState)))
	{
		info.substate = MANUAL_WAIT_FOR_MANUAL;
		info.timeoutsSet = false;
	}
}

inline void manualWaitForManual()
{
	subSystemStates.sacState = getFullCSState(SA_STATE_UNAVAILABLE, 0);
	subSystemStates.sbState = getFullCSState(SB_STATE_UNAVAILABLE, 0);
	subSystemStates.cacState = getFullCSState(CA_STATE_UNAVAILABLE, 0);
	
	if (!info.timeoutsSet)
	{
		timeoutsAssc.sac = STANDARD_TIMEOUT_MS;
		timeoutsAssc.sb = STANDARD_TIMEOUT_MS;
		timeoutsAssc.cac = STANDARD_TIMEOUT_MS;
		info.timeoutsSet = true;
	}
	
	if ((getCSState(subSystemStates.sacState) == getCSState(subSystemStatesReceive.sacState))
		&& (getCSState(subSystemStates.sbState) == getCSState(subSystemStatesReceive.sbState))
		&& (getCSState(subSystemStates.cacState) == getCSState(subSystemStatesReceive.cacState)))
		//&& !tsIsOff())
		//&& !r2dIsOff())
	{
		info.state = AS_STATE_MANUAL;
		info.substate = 0;
		info.timeoutsSet = false;
	}
}

inline void asWaitAsms()
{
	if (info.asmsOn)
	{
		info.substate = AS_CHECK_BRAKE_PRESSURE_LOW;
		info.timeoutsSet = false;
	}
}

inline void asCheckBrakePressure()
{
	subSystemStates.ebsState = getFullCSState(EBS_STATE_ARMED, EBS_SUBSTATE_WAIT_BRAKE_REAR_ENGAGED);
	subSystemStates.hscState = getFullCSState(HS_STATE_UNAVAILABLE, 0);
	
	if (!info.timeoutsSet)
	{
		timeoutsAssc.ebsSubstate = STANDARD_TIMEOUT_MS;
		timeoutsAssc.hsc = STANDARD_TIMEOUT_MS;
		info.timeoutsSet = true;
	}

	if ((getCSSubstate(subSystemStates.ebsState) == getCSSubstate(subSystemStatesReceive.ebsState))
		&& (getCSState(subSystemStates.hscState) == getCSState(subSystemStatesReceive.hscState)))
	{
		info.substate = AS_TURN_ON_PUMP;
		info.timeoutsSet = false;
	}
}

inline void asTurnOnPump()
{
	subSystemStates.hscState = getFullCSState(HS_STATE_AVAILABLE, 0);
	
	if (!info.timeoutsSet)
	{
		timeoutsAssc.hsc = STANDARD_TIMEOUT_MS;
		info.timeoutsSet = true;
	}
	
	if ((getCSState(subSystemStates.hscState) == getCSState(subSystemStatesReceive.hscState)))
	{
		info.substate = AS_WAIT_EBS_ETC_CAC;
		info.timeoutsSet = false;
	}
}

inline void asWaitEbsEtcCac()
{
	subSystemStates.ebsState = getFullCSState(EBS_STATE_ARMED, 0);
	subSystemStates.etcState = getFullCSState(ETC_STATE_AUTONOMOUS, 0);
	subSystemStates.cacState = getFullCSState(CA_STATE_ENGAGED, 0);
	
	if (!info.timeoutsSet)
	{
		timeoutsAssc.ebs = STANDARD_TIMEOUT_MS;
		timeoutsAssc.etc = STANDARD_TIMEOUT_MS;
		timeoutsAssc.cac = STANDARD_TIMEOUT_MS;
		info.timeoutsSet = true;
	}
	
	if ((getCSState(subSystemStates.ebsState) == getCSState(subSystemStatesReceive.ebsState))
		&& (getCSState(subSystemStates.etcState) == getCSState(subSystemStatesReceive.etcState))
		&& (getCSState(subSystemStates.cacState) == getCSState(subSystemStatesReceive.cacState))
		&& info.asmsOn)
	{
		info.substate = AS_WAIT_BRAKE_ENGAGED;
		info.timeoutsSet = false;
	}
}

inline void asWaitBrakeEngaged()
{
	subSystemStates.sbState = getFullCSState(SB_STATE_ENGAGED, 0);
	
	if (!info.timeoutsSet)
	{
		timeoutsAssc.sb = STANDARD_TIMEOUT_MS;
		info.timeoutsSet = true;
	}
	
	if ((getCSState(subSystemStates.sbState) == getCSState(subSystemStatesReceive.sbState)) && info.camera)
	{
		info.substate = AS_WAIT_TRACTIVE;
		info.timeoutsSet = false;
	}
}

inline void asWaitForTractive()
{	
	if (!tsIsOff())
	{
		info.substate = AS_WAIT_FOR_READY;
		info.timeoutsSet = false;
	}
}

inline void asWaitForReady()
{
	subSystemStates.ebsState = getFullCSState(EBS_STATE_ARMED, 0);
	subSystemStates.etcState = getFullCSState(ETC_STATE_AUTONOMOUS, 0);
	subSystemStates.cacState = getFullCSState(CA_STATE_ENGAGED, 0);
	subSystemStates.sacState = getFullCSState(SA_STATE_AVAILABLE, 0);
	subSystemStates.sbState = getFullCSState(SB_STATE_ENGAGED, 0);
	
	if (!info.timeoutsSet)
	{
		timeoutsAssc.ebs = STANDARD_TIMEOUT_MS;
		timeoutsAssc.etc = STANDARD_TIMEOUT_MS;
		timeoutsAssc.cac = STANDARD_TIMEOUT_MS;
		timeoutsAssc.sac = STANDARD_TIMEOUT_MS;
		timeoutsAssc.sb = STANDARD_TIMEOUT_MS;
		info.timeoutsSet = true;
	}
	
	if ((getCSState(subSystemStates.ebsState) == getCSState(subSystemStatesReceive.ebsState))
		&& (getCSState(subSystemStates.etcState) == getCSState(subSystemStatesReceive.etcState))
		&& (getCSState(subSystemStates.cacState) == getCSState(subSystemStatesReceive.cacState))
		&& (getCSState(subSystemStates.sacState) == getCSState(subSystemStatesReceive.sacState))
		&& (getCSState(subSystemStates.sbState) == getCSState(subSystemStatesReceive.sbState))
		&& info.asmsOn)
		//&& !tsIsOff())
	{
		info.state = AS_STATE_READY;
		info.substate = WAIT_5_SECONDS;
		info.timeoutsSet = false;
	}
}

void off()
{
	setStateLED(MAGENTA);
	
	info.assiColor = ASSI_COLOR_OFF;
	info.assiMode = ASSI_MODE_STEADY;
	info.buzzerMode = ASSI_BUZZER_OFF;
	info.assiRulesState = ASSI_STATE_OFF;
	
	if(gpio_get_pin_level(EBS_FAILURE_LIGHT))
	{
		info.substate = DEAD_END;
	}
	
	switch(info.substate)
	{
		case WAIT_MISSION: waitMission(); break;
		case MANUAL_WAIT_TRACTIVE: manualWaitTractive(); break;
		case MANUAL_WAIT_FOR_MANUAL: manualWaitForManual(); break;
		case AS_WAIT_ASMS: asWaitAsms(); break;
		case AS_CHECK_BRAKE_PRESSURE_LOW: asCheckBrakePressure(); break;
		case AS_TURN_ON_PUMP: asTurnOnPump(); break;
		case AS_WAIT_EBS_ETC_CAC: asWaitEbsEtcCac(); break;
		case AS_WAIT_BRAKE_ENGAGED: asWaitBrakeEngaged(); break;
		case AS_WAIT_TRACTIVE: asWaitForTractive(); break;
		case AS_WAIT_FOR_READY: asWaitForReady(); break;
		case DEAD_END: deadEnd(); break;
	}
	
	if (info.substate == WAIT_MISSION)
	{
		subSystemStates.amiState = getFullCSState(AMI_STATE_NORMAL, AMI_SUBSTATE_NORMAL);
		return;
	}
	
	if(!info.shutDownCircuitClosed && info.substate != WAIT_MISSION)
	{
		if (info.asmsOn)
		{
			subSystemStates.amiState = getFullCSState(AMI_STATE_SHOW_RESTART, AMI_SUBSTATE_CLOSE_SC);
		}
		else
		{
			subSystemStates.amiState = getFullCSState(AMI_STATE_SHOW_RESTART, 3);
		}
	}
	
	if(info.shutDownCircuitClosed && info.substate != WAIT_MISSION)
	{
		switch (info.substate)
		{
			case AS_WAIT_ASMS: subSystemStates.amiState = getFullCSState(AMI_STATE_SHOW_RESTART, 3); break;
			case AS_CHECK_BRAKE_PRESSURE_LOW:
			case AS_TURN_ON_PUMP:
			case AS_WAIT_EBS_ETC_CAC: subSystemStates.amiState = getFullCSState(AMI_STATE_SHOW_RESTART, 4); break;
			case AS_WAIT_TRACTIVE: subSystemStates.amiState = getFullCSState(AMI_STATE_SHOW_RESTART, AMI_SUBSTATE_ENABLE_STARTER); break;
			case AS_WAIT_FOR_READY: subSystemStates.amiState = getFullCSState(AMI_STATE_LOCKED, AMI_SUBSTATE_ENABLE_STARTER); break;
			default: subSystemStates.amiState = getFullCSState(AMI_STATE_LOCKED, AMI_SUBSTATE_ENABLE_STARTER); break;
		}
	}
}