/*
 * ebs.c
 *
 * Created: 18.05.2019 17:53:14
 *  Author: driverless
 */ 

#include "states.h"

static struct timer_task buzzerTimer;
bool buzzerTimerInitialized = false;
int buzzerTimerCounter = 80; // TODO set to 80 (for 20 seconds)

static void buzzerTimerTask(const struct timer_task *const timer_task)
{
	gpio_toggle_pin_level(ACTIVATE_EBS);
	
	buzzerTimerCounter--;
	if (buzzerTimerCounter == 0)
	{
		timer_remove_task(&TIMER_0, &buzzerTimer);
		gpio_set_pin_level(ACTIVATE_EBS, true);
		info.buzzerActive = false;
	}
}

void triggerEBS()
{
	gpio_set_pin_level(ACTIVATE_EBS, false);
	info.triggeredShutdownCircuit = true;
}

void ebs()
{
	setStateLED(RED);
	
	info.assiColor = ASSI_COLOR_BLUE;
	info.assiMode = ASSI_MODE_FLASHING;
	info.buzzerMode = ASSI_BUZZER_ON;
	info.assiRulesState = ASSI_STATE_BLUE_FLASH,
	subSystemStates.amiState = getFullCSState(AMI_STATE_SHOW_RESTART, AMI_SUBSTATE_NORMAL);
	subSystemStates.etcState = getFullCSState(ETC_STATE_OFF, 0);
	subSystemStates.hscState = getFullCSState(HS_STATE_AVAILABLE, 0);
	subSystemStates.sacState = getFullCSState(SA_STATE_AVAILABLE, 0);
	subSystemStates.cacState = getFullCSState(CA_STATE_ENGAGED, 0);
	subSystemStates.ebsState = getFullCSState(EBS_STATE_ACTIVATED, 0);
	subSystemStates.sbState = getFullCSState(SB_STATE_ENGAGED, 0);
	
	if (!info.triggeredShutdownCircuit)
	{
		triggerEBS();
	}
	
	if (!buzzerTimerInitialized)
	{
		buzzerTimerInitialized = true;
		info.buzzerActive = true;
		
		buzzerTimer.interval = 250;
		buzzerTimer.mode = TIMER_TASK_REPEAT;
		buzzerTimer.cb = buzzerTimerTask;
		
		timer_add_task(&TIMER_0, &buzzerTimer);
	}
}