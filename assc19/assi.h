/*
 * assi.h
 *
 * Created: 18.05.2019 18:32:07
 *  Author: driverless
 */ 


#ifndef ASSI_H_
#define ASSI_H_

#include "driver_init.h"
#include "global.h"

struct timer_task assiTask;

void initASSI();
void setASSIYellow();
void setASSIBlue();
void setASSIOff();

void assiTaskCallback(const struct timer_task *const timer_task);

#endif /* ASSI_H_ */