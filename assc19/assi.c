/*
 * assi.c
 *
 * Created: 18.05.2019 18:36:00
 *  Author: driverless
 */ 

#include "assi.h"

int assiCounter = 0;
int skipCounter = 0;
int onTime = 5;
int maxOnTime = 10;

void initASSI()
{
	assiTask.interval = 10;
	assiTask.mode = TIMER_TASK_REPEAT;
	assiTask.cb = assiTaskCallback;
	
	timer_add_task(&TIMER_0, &assiTask);
}

void setASSIYellow()
{
	gpio_set_pin_level(EN_ASSI_YELLOW, true);
	gpio_set_pin_level(EN_ASSI_BLUE, false);
}

void setASSIBlue()
{
	gpio_set_pin_level(EN_ASSI_YELLOW, false);
	gpio_set_pin_level(EN_ASSI_BLUE, true);
}

void setASSIOff()
{
	gpio_set_pin_level(EN_ASSI_YELLOW, false);
	gpio_set_pin_level(EN_ASSI_BLUE, false);
}

void assiTaskCallback(const struct timer_task *const timer_task)
{	
	
	if (skipCounter == 0)
	{
		if (assiCounter > onTime)
		{
			if (assiCounter > maxOnTime)
			{
				assiCounter = 0;
			}
			
			if (info.assiMode == ASSI_MODE_FLASHING)
			{
				setASSIOff();
			}
			
			if (info.assiMode == ASSI_MODE_PARTY)
			{
				setASSIBlue();
			}
			
			} else {
			
			if (info.assiMode == ASSI_MODE_PARTY)
			{
				setASSIYellow();
				} else {
				if (info.assiColor == ASSI_COLOR_OFF)
				{
					setASSIOff();
				}
				
				if (info.assiColor == ASSI_COLOR_YELLOW)
				{
					setASSIYellow();
				}
				
				if (info.assiColor == ASSI_COLOR_BLUE)
				{
					setASSIBlue();
				}
			}
		}
		
		assiCounter++;
	}
	
	skipCounter++;
	if (skipCounter > 5)
	{
		skipCounter = 0;
	}
}