#include <atmel_start.h>
#include "armcustomdrivers/canInterface/canMsgBuilder.h"
#include "states/states.h"
#include "assi.h"
#include "armcustomdrivers/utilities/averaging.h"

static struct timer_task watchDog;
static struct timer_task resStartup;
static struct timer_task timeoutCounter;
static struct timer_task tsOnUpdate;
static struct timer_task assiCheck;

struct can_filter resFilter;
struct can_filter dashFilter;

void updateSubStates(struct can_message *msg)
{
	if ((msg->id & MESSAGE_FILTER) == AS_SYSLOG_CAN_MSG_ID_OFFSET)
	{
		switch(msg->id & DEVICE_ID_FILTER)
		{
			case DEV_ID_ASSB: subSystemStatesReceive.sbState = msg->data[0]; memcpy(syslogData.sb, msg->data, 8); break;
			case DEV_ID_ASSAC: subSystemStatesReceive.sacState = msg->data[0]; memcpy(syslogData.sac, msg->data, 8); break;
			case DEV_ID_ASCAC: subSystemStatesReceive.cacState = msg->data[0]; memcpy(syslogData.cac, msg->data, 8); break;
			case DEV_ID_ASETC: subSystemStatesReceive.etcState = msg->data[0]; memcpy(syslogData.etc, msg->data, 8); break;
			case DEV_ID_ASHSC: subSystemStatesReceive.hscState = msg->data[0]; memcpy(syslogData.hsc, msg->data, 8); break;
			case DEV_ID_ASCPU: subSystemStatesReceive.cpuState = msg->data[0]; memcpy(syslogData.ascpu, msg->data, 8); info.camera = ((msg->data[6] >> 7) & 0b1); info.lidar = ((msg->data[6] >> 6) & 0b1); info.imu = ((msg->data[6] >> 5) & 0b1); break;
			case DEV_ID_AMI: subSystemStatesReceive.amiState = msg->data[0]; memcpy(syslogData.ami, msg->data, 8); break;
			case DEV_ID_ASEBS: subSystemStatesReceive.ebsState = msg->data[0]; memcpy(syslogData.ebs, msg->data, 8); break;
			default: break;
		}
	}
}

inline void updateCounter(int *counter)
{
	if (*counter > 0)
	{
		(*counter)--;
	}
}

void canCallback(struct can_message *msg)
{	
	updateSubStates(msg);
	
	switch(msg->id)
	{
		case MSG_ID_RES:
		{
			if (!info.resBoxRunning)
			{
				timer_remove_task(&TIMER_0, &resStartup);
				info.resBoxRunning = true;
			}
			
			if(msg->data[3] == 0 && info.state != AS_STATE_OFF && info.state != AS_STATE_MANUAL)
			{
				info.state = AS_STATE_EBS;
				info.substate = 1;
			}
			
			if (!info.goState && info.state == AS_STATE_READY)
			{
				info.goState = ((msg->data[0] >> 2) & 0b1);
			}
			
			break;
		}
		case MSG_ID_DASH:
		{
			info.rpm = to16bit(msg->data[3], msg->data[2]);
			info.vehicleSpeed = to16bit(msg->data[5], msg->data[4]);
			info.gear = msg->data[1] - 2;
			break;
		}
		default: break;
	}
}

static void watchDogTask(const struct timer_task *const timer_task)
{
	gpio_toggle_pin_level(WD_OUTPUT);
}

static void resStartupTask(const struct timer_task *const timer_task)
{
	can_async_write(&CAN_0, &resStartupMsg);
}

static void timeoutCounterTask(const struct timer_task *const timer_task)
{
	updateCounter(&timeoutsAssc.sb);
	updateCounter(&timeoutsAssc.sac);
	updateCounter(&timeoutsAssc.cac);
	updateCounter(&timeoutsAssc.etc);
	updateCounter(&timeoutsAssc.hsc);
	updateCounter(&timeoutsAssc.ebs);
	updateCounter(&timeoutsAssc.ebsSubstate);
}

static void tsUpdateTask(const struct timer_task *const timer_task)
{
	if (info.rpm > 3000)
	{
		info.tsIsOnCounter++;
	}
	
	if (info.rpm < 1000)
	{
		info.tsIsOnCounter = 0;
		info.tsIsOn = false;
	}
	
	if (info.tsIsOnCounter >= 30)
	{
		info.tsIsOn = true;
	}
}

volatile uint8_t yellowCounter = 0;
volatile uint8_t blueCounter = 0;
volatile uint8_t yellowCounterConst = 0;
volatile uint8_t blueCounterConst = 0;
volatile uint8_t prevState = 1;

bool checkYellowAssiBlink() {
	//assi yellow blink check
	uint8_t tempState = 0;
	
	if(sensorHolder.yellowASSIDiag->value < 50) 
	{
		tempState = 1;
	} 
	else if(sensorHolder.yellowASSIDiag->value > (350 - ((35*(16000 - sensorHolder.cDiag->value))>>10))) 
	{
		tempState = 2;
	}
	
	
	if(prevState == tempState) {
		yellowCounter++;
	} else if(tempState != 0) {
		prevState = tempState;
		yellowCounter = 0;
	} else {
		yellowCounter++;
	}
	
	if(yellowCounter > 7) {
		//failure
		return false;
	}
	
	return true;
}

bool checkYellowAssiSteady() {
	//assi yellow blink check
	uint8_t tempState = 0;
	
	if(sensorHolder.yellowASSIDiag->value < 50)
	{
		tempState = 1;
	}
	else if(sensorHolder.yellowASSIDiag->value > (350 - ((35*(16000 - sensorHolder.cDiag->value))>>10)))
	{
		tempState = 2;
	}
	
	
	if(tempState != 2) {
		yellowCounterConst++;
	} else {
		yellowCounterConst = 0;
	}
	
	if(yellowCounterConst > 7) {
		//failure
		return false;
	}
	
	return true;
}

bool checkBlueAssiBlink() {
	//assi yellow blink check
	uint8_t tempState = 0;
	
	if(sensorHolder.blueASSIDiag->value < 50)
	{
		tempState = 1;
	}
	else if(sensorHolder.blueASSIDiag->value > (340 - ((42*(16000 - sensorHolder.cDiag->value))>>10)))
	{
		tempState = 2;
	}
	
	
	if(prevState == tempState) {
		blueCounter++;
		} else if(tempState != 0) {
		prevState = tempState;
		blueCounter = 0;
		} else {
		blueCounter++;
	}
	
	if(blueCounter > 7) {
		//failure
		return false;
	}
	
	return true;
}

bool checkBlueAssiSteady() {
	//assi yellow blink check
	uint8_t tempState = 0;
	
	if(sensorHolder.blueASSIDiag->value < 50)
	{
		tempState = 1;
	}
	else if(sensorHolder.blueASSIDiag->value > (340 - ((42*(16000 - sensorHolder.cDiag->value))>>10)))
	{
		tempState = 2;
	}
	
	
	if(tempState != 2) {
		blueCounterConst++;
	} else {
		blueCounterConst = 0;
	}
	
	if(blueCounterConst > 7) {
		//failure
		return false;
	}
	
	return true;
}



static void assiCheckTask(const struct timer_task *const timer_task)
{	
	/*if (info.state == AS_STATE_OFF || info.state == AS_STATE_MANUAL)
	{
		return;
	}*/
	
	if(info.assiColor == ASSI_COLOR_YELLOW && info.assiMode == ASSI_MODE_FLASHING) {
		info.assiError = !checkYellowAssiBlink();
	} else if(info.assiColor == ASSI_COLOR_BLUE && info.assiMode == ASSI_MODE_FLASHING) {
		info.assiError = !checkBlueAssiBlink();
	} else if(info.assiColor == ASSI_COLOR_YELLOW && info.assiMode == ASSI_MODE_STEADY) {
		info.assiError = !checkYellowAssiSteady();
	} else if(info.assiColor == ASSI_COLOR_BLUE && info.assiMode == ASSI_MODE_STEADY) {
		info.assiError = !checkBlueAssiSteady();
	}
}

static void asmsHandler(void)
{
	info.asmsOn = gpio_get_pin_level(ASMS_IS_ACTIVE);
}

static void scHandler(void)
{
	uint8_t sc = (gpio_get_pin_level(SC_STATUS_2) << 2) | (gpio_get_pin_level(SC_STATUS_1) << 1) | (gpio_get_pin_level(SC_STATUS_0));
	info.scStatus0 = gpio_get_pin_level(SC_STATUS_0);
	info.scStatus1 = gpio_get_pin_level(SC_STATUS_1);
	info.scStatus2 = gpio_get_pin_level(SC_STATUS_2);
	
	if (sc == SC_CLOSED && !info.triggeredShutdownCircuit)
	{
		info.shutDownCircuitClosed = true;
	} else {
		info.shutDownCircuitClosed = false;
	}
}

void initEBS()
{
	gpio_set_pin_level(ACTIVATE_EBS, true);
	gpio_set_pin_level(EBS_FAILURE_LIGHT, false);
	info.triggeredShutdownCircuit = false;
}

void initSensors()
{
	sensorHolder.aDiag = initCustomSensor(SENSOR_AN0, 
										SENSOR_DIAG_STANDARD_GRADIENT, 
										SENSOR_DIAG_STANDARD_SHIFT, 
										SENSOR_DIAG_STANDARD_OFFSET, 
										SENSOR_DIAG_STANDARD_MINIMUM, 
										SENSOR_DIAG_STANDARD_MAXIMUM, 
										SENSOR_DIAG_STANDARD_LOWER_THRESHOLD, 
										SENSOR_DIAG_STANDARD_UPPER_THRESHOLD, 
										0);
	loadSensorConfig(sensorHolder.aDiag);
	
	sensorHolder.cDiag = initCustomSensor(SENSOR_AN1,
										SENSOR_DIAG_STANDARD_GRADIENT,
										SENSOR_DIAG_STANDARD_SHIFT,
										SENSOR_DIAG_STANDARD_OFFSET,
										SENSOR_DIAG_STANDARD_MINIMUM,
										SENSOR_DIAG_STANDARD_MAXIMUM,
										SENSOR_DIAG_STANDARD_LOWER_THRESHOLD,
										SENSOR_DIAG_STANDARD_UPPER_THRESHOLD,
										0);
	loadSensorConfig(sensorHolder.cDiag);
	
	sensorHolder.sensor1Diag = initCustomSensor(SENSOR_AN5,
										SENSOR_DIAG_STANDARD_GRADIENT,
										SENSOR_DIAG_STANDARD_SHIFT,
										SENSOR_DIAG_STANDARD_OFFSET,
										SENSOR_DIAG_STANDARD_MINIMUM,
										SENSOR_DIAG_STANDARD_MAXIMUM,
										SENSOR_DIAG_STANDARD_LOWER_THRESHOLD,
										SENSOR_DIAG_STANDARD_UPPER_THRESHOLD,
										0);
	loadSensorConfig(sensorHolder.sensor1Diag);
										
	sensorHolder.sensor2Diag = initCustomSensor(SENSOR_AN4,
										SENSOR_DIAG_STANDARD_GRADIENT,
										SENSOR_DIAG_STANDARD_SHIFT,
										SENSOR_DIAG_STANDARD_OFFSET,
										SENSOR_DIAG_STANDARD_MINIMUM,
										SENSOR_DIAG_STANDARD_MAXIMUM,
										SENSOR_DIAG_STANDARD_LOWER_THRESHOLD,
										SENSOR_DIAG_STANDARD_UPPER_THRESHOLD,
										0);
	loadSensorConfig(sensorHolder.sensor2Diag);
										
	sensorHolder.scIRDiag = initCustomSensor(SENSOR_AN8,
										SENSOR_DIAG_STANDARD_GRADIENT,
										SENSOR_DIAG_STANDARD_SHIFT,
										SENSOR_DIAG_STANDARD_OFFSET,
										SENSOR_DIAG_STANDARD_MINIMUM,
										SENSOR_DIAG_STANDARD_MAXIMUM,
										SENSOR_DIAG_STANDARD_LOWER_THRESHOLD,
										SENSOR_DIAG_STANDARD_UPPER_THRESHOLD,
										0);
	loadSensorConfig(sensorHolder.scIRDiag);
										
	sensorHolder.scEBSRDiag = initCustomSensor(SENSOR_AN11,
										SENSOR_DIAG_STANDARD_GRADIENT,
										SENSOR_DIAG_STANDARD_SHIFT,
										SENSOR_DIAG_STANDARD_OFFSET,
										SENSOR_DIAG_STANDARD_MINIMUM,
										SENSOR_DIAG_STANDARD_MAXIMUM,
										SENSOR_DIAG_STANDARD_LOWER_THRESHOLD,
										SENSOR_DIAG_STANDARD_UPPER_THRESHOLD,
										0);
	loadSensorConfig(sensorHolder.scEBSRDiag);
										
	sensorHolder.ebsDiag = initCustomSensor(SENSOR_AN10,
										SENSOR_DIAG_STANDARD_GRADIENT,
										SENSOR_DIAG_STANDARD_SHIFT,
										SENSOR_DIAG_STANDARD_OFFSET,
										SENSOR_DIAG_STANDARD_MINIMUM,
										SENSOR_DIAG_STANDARD_MAXIMUM,
										SENSOR_DIAG_STANDARD_LOWER_THRESHOLD,
										SENSOR_DIAG_STANDARD_UPPER_THRESHOLD,
										0);
	loadSensorConfig(sensorHolder.ebsDiag);
										
	sensorHolder.scFPRDiag = initCustomSensor(SENSOR_AN6,
										SENSOR_DIAG_STANDARD_GRADIENT,
										SENSOR_DIAG_STANDARD_SHIFT,
										SENSOR_DIAG_STANDARD_OFFSET,
										SENSOR_DIAG_STANDARD_MINIMUM,
										SENSOR_DIAG_STANDARD_MAXIMUM,
										SENSOR_DIAG_STANDARD_LOWER_THRESHOLD,
										SENSOR_DIAG_STANDARD_UPPER_THRESHOLD,
										0);
	loadSensorConfig(sensorHolder.scFPRDiag);
										
	sensorHolder.blueASSIDiag = initStandardSensor(SENSOR_AN9, SENSOR_BTS5200);
	loadSensorConfig(sensorHolder.blueASSIDiag);
	
	sensorHolder.yellowASSIDiag = initStandardSensor(SENSOR_AN7, SENSOR_BTS5200);
	loadSensorConfig(sensorHolder.yellowASSIDiag);
	
	
	initSensorReading();
	initSensorDataCANMessages(cmb, sensorHolder.aDiag,
								sensorHolder.ebsDiag,
								sensorHolder.cDiag,
								sensorHolder.sensor1Diag,
								sensorHolder.sensor2Diag,
								sensorHolder.yellowASSIDiag,
								sensorHolder.blueASSIDiag,
								sensorHolder.scEBSRDiag,
								sensorHolder.scFPRDiag,
								sensorHolder.scIRDiag,
								0, 0);
	initCANCalibration(cmb);
}

void syslogCallback(struct can_message *msg)
{
	msg->data[3] = (errorStates.ascpu << 7)
				| (errorStates.ebs << 6)
				| (errorStates.ami << 5)
				| (errorStates.sb << 4)
				| (errorStates.sac << 3)
				| (errorStates.cac << 2)
				| (errorStates.etc << 1)
				| (errorStates.hsc);
	msg->data[6] = (info.scStatus2 << 7) | (info.scStatus1 << 6) | (info.scStatus0 << 5) | (info.goState << 4);
	msg->data[7] = (info.asmsOn << 7) 
				| (true << 6) 
				| (gpio_get_pin_level(ACTIVATE_EBS) << 5) 
				| (gpio_get_pin_level(EBS_FAILURE_LIGHT) << 4) 
				| ((info.assiRulesState & ASSI_STATE_MASK) << 1) 
				| (info.buzzerActive);
}

void manualControlCallback(struct can_message *msg)
{
	if ((msg->data[1] & 0b1) == 1)
	{
		disableASStateMessage();
		info.disableChecks = true;
	} else {
		enableASStateMessage();
		info.disableChecks = false;
	}
}

/************************************************************************/
/* jump table definition                                                */
/************************************************************************/
static void (* const checkStates[STATE_COUNT])(void) = {off, ready, driving, finished, ebs, manual, light};

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	
	initEBS();
	
	watchDog.interval = 16;
	watchDog.mode = TIMER_TASK_REPEAT;
	watchDog.cb = watchDogTask;
	gpio_set_pin_level(WD_OUTPUT, true);
	timer_add_task(&TIMER_0, &watchDog);
	
	timer_start(&TIMER_0);
	
	initStateLED();
	initASSI();
	initResMessage();
	
	initCanMsgBuilder(&TIMER_0, canCallback);
	cmb = addCANMsgBuilderInstance(DEV_ID_ASSC);
	
	initASStateCANMessage(&subSystemStates);
	initSystemCANMessage(cmb, &info.state, &info.substate, syslogCallback, 0);
	initSensors();
	initCANManualControl(cmb, manualControlCallback);
	
	resFilter.id = 0x191;
	resFilter.mask = FULL_FILTERING;
	can_async_set_filter(&CAN_0, CAN_RX_FILTER_INDEX_TARGET_VALUE_MSG, CAN_FMT_STDID, &resFilter);
	
	dashFilter.id = 0x201;
	dashFilter.mask = FULL_FILTERING;
	can_async_set_filter(&CAN_0, CAN_RX_FILTER_INDEX_ETC_DBW_TARGET_MSG, CAN_FMT_STDID, &dashFilter);

	/* res startup message init */
	resStartup.interval = 100;
	resStartup.mode = TIMER_TASK_REPEAT;
	resStartup.cb = resStartupTask;
	timer_add_task(&TIMER_0, &resStartup);
	
	/* timeout counter init */
	timeoutCounter.interval = 1;
	timeoutCounter.mode = TIMER_TASK_REPEAT;
	timeoutCounter.cb = timeoutCounterTask;
	timer_add_task(&TIMER_0, &timeoutCounter);
	
	tsOnUpdate.interval = 100;
	tsOnUpdate.mode = TIMER_TASK_REPEAT;
	tsOnUpdate.cb = tsUpdateTask;
	//timer_add_task(&TIMER_0, &tsOnUpdate);
	
	assiCheck.interval = 100;
	assiCheck.mode = TIMER_TASK_REPEAT;
	assiCheck.cb = assiCheckTask;
	timer_add_task(&TIMER_0, &assiCheck);
	
	/* initialize asms and sc variables */
	asmsHandler();
	scHandler();
	
	ext_irq_register(ASMS_IS_ACTIVE, asmsHandler);
	ext_irq_register(SC_STATUS_0, scHandler);
	ext_irq_register(SC_STATUS_1, scHandler);
	ext_irq_register(SC_STATUS_2, scHandler);

	/* Replace with your application code */
	while (1) 
	{
		if (!info.disableChecks)
		{
			checkSubSystemStates();
			checkResAsmsAssi();
			checkEBSsensorBits();
		}
		checkStates[info.state]();
		subSystemStates.asState = getFullASState(info.state, info.substate);
	}
}
