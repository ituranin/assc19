/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */

#include "driver_init.h"
#include <peripheral_clk_config.h>
#include <utils.h>
#include <hal_init.h>

#include <hpl_adc_base.h>

/* The channel amount for ADC */
#define ADC_0_CH_AMOUNT 1

/* The buffer size for ADC */
#define ADC_0_BUFFER_SIZE 16

/* The maximal channel number of enabled channels */
#define ADC_0_CH_MAX 0

struct adc_async_descriptor         ADC_0;
struct adc_async_channel_descriptor ADC_0_ch[ADC_0_CH_AMOUNT];
struct timer_descriptor             TIMER_0;
struct can_async_descriptor         CAN_0;

static uint8_t ADC_0_buffer[ADC_0_BUFFER_SIZE];
static uint8_t ADC_0_map[ADC_0_CH_MAX + 1];

struct flash_descriptor FLASH_0;

/**
 * \brief ADC initialization function
 *
 * Enables ADC peripheral, clocks and initializes ADC driver
 */
void ADC_0_init(void)
{
	hri_mclk_set_APBCMASK_ADC0_bit(MCLK);
	hri_gclk_write_PCHCTRL_reg(GCLK, ADC0_GCLK_ID, CONF_GCLK_ADC0_SRC | (1 << GCLK_PCHCTRL_CHEN_Pos));
	adc_async_init(&ADC_0, ADC0, ADC_0_map, ADC_0_CH_MAX, ADC_0_CH_AMOUNT, &ADC_0_ch[0], _adc_get_adc_async());
	adc_async_register_channel_buffer(&ADC_0, 0, ADC_0_buffer, ADC_0_BUFFER_SIZE);
}

void EXTERNAL_IRQ_0_init(void)
{
	hri_gclk_write_PCHCTRL_reg(GCLK, EIC_GCLK_ID, CONF_GCLK_EIC_SRC | (1 << GCLK_PCHCTRL_CHEN_Pos));
	hri_mclk_set_APBAMASK_EIC_bit(MCLK);

	// Set pin direction to input
	gpio_set_pin_direction(ASMS_IS_ACTIVE, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(ASMS_IS_ACTIVE,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(ASMS_IS_ACTIVE, PINMUX_PA19A_EIC_EXTINT3);

	// Set pin direction to input
	gpio_set_pin_direction(SC_STATUS_2, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(SC_STATUS_2,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(SC_STATUS_2, PINMUX_PA22A_EIC_EXTINT6);

	// Set pin direction to input
	gpio_set_pin_direction(SC_STATUS_1, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(SC_STATUS_1,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(SC_STATUS_1, PINMUX_PA23A_EIC_EXTINT7);

	// Set pin direction to input
	gpio_set_pin_direction(SC_STATUS_0, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(SC_STATUS_0,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(SC_STATUS_0, PINMUX_PA27A_EIC_EXTINT15);

	ext_irq_init();
}

void FLASH_0_CLOCK_init(void)
{

	hri_mclk_set_AHBMASK_NVMCTRL_bit(MCLK);
}

void FLASH_0_init(void)
{
	FLASH_0_CLOCK_init();
	flash_init(&FLASH_0, NVMCTRL);
}

void delay_driver_init(void)
{
	delay_init(SysTick);
}

/**
 * \brief Timer initialization function
 *
 * Enables Timer peripheral, clocks and initializes Timer driver
 */
static void TIMER_0_init(void)
{
	hri_mclk_set_APBCMASK_TC0_bit(MCLK);
	hri_gclk_write_PCHCTRL_reg(GCLK, TC0_GCLK_ID, CONF_GCLK_TC0_SRC | (1 << GCLK_PCHCTRL_CHEN_Pos));

	timer_init(&TIMER_0, TC0, _tc_get_timer());
}

void CAN_0_PORT_init(void)
{

	gpio_set_pin_function(CAN_RX, PINMUX_PA25G_CAN0_RX);

	gpio_set_pin_function(CAN_TX, PINMUX_PA24G_CAN0_TX);
}
/**
 * \brief CAN initialization function
 *
 * Enables CAN peripheral, clocks and initializes CAN driver
 */
void CAN_0_init(void)
{
	hri_mclk_set_AHBMASK_CAN0_bit(MCLK);
	hri_gclk_write_PCHCTRL_reg(GCLK, CAN0_GCLK_ID, CONF_GCLK_CAN0_SRC | (1 << GCLK_PCHCTRL_CHEN_Pos));
	can_async_init(&CAN_0, CAN0);
	CAN_0_PORT_init();
}

void system_init(void)
{
	init_mcu();

	// GPIO on PA02

	// Disable digital pin circuitry
	gpio_set_pin_direction(VCC_ACT_DIAG, GPIO_DIRECTION_OFF);

	gpio_set_pin_function(VCC_ACT_DIAG, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA03

	// Disable digital pin circuitry
	gpio_set_pin_direction(VCC_SYSTEM_DIAG, GPIO_DIRECTION_OFF);

	gpio_set_pin_function(VCC_SYSTEM_DIAG, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA04

	// Disable digital pin circuitry
	gpio_set_pin_direction(SENSOR_2_DIAG, GPIO_DIRECTION_OFF);

	gpio_set_pin_function(SENSOR_2_DIAG, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA05

	// Disable digital pin circuitry
	gpio_set_pin_direction(SENSOR_1_DIAG, GPIO_DIRECTION_OFF);

	gpio_set_pin_function(SENSOR_1_DIAG, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA06

	// Disable digital pin circuitry
	gpio_set_pin_direction(SC_FP_REL_DIAG, GPIO_DIRECTION_OFF);

	gpio_set_pin_function(SC_FP_REL_DIAG, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA07

	// Disable digital pin circuitry
	gpio_set_pin_direction(ASSI_YELLOW_DIAG, GPIO_DIRECTION_OFF);

	gpio_set_pin_function(ASSI_YELLOW_DIAG, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA08

	// Disable digital pin circuitry
	gpio_set_pin_direction(SC_IGN_REL_DIAG, GPIO_DIRECTION_OFF);

	gpio_set_pin_function(SC_IGN_REL_DIAG, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA09

	// Disable digital pin circuitry
	gpio_set_pin_direction(ASSI_BLUE_DIAG, GPIO_DIRECTION_OFF);

	gpio_set_pin_function(ASSI_BLUE_DIAG, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA10

	// Disable digital pin circuitry
	gpio_set_pin_direction(VCC_EBS_DIAG, GPIO_DIRECTION_OFF);

	gpio_set_pin_function(VCC_EBS_DIAG, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA11

	// Disable digital pin circuitry
	gpio_set_pin_direction(SC_EBS_REL_DIAG, GPIO_DIRECTION_OFF);

	gpio_set_pin_function(SC_EBS_REL_DIAG, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA14

	gpio_set_pin_level(WD_OUTPUT,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(WD_OUTPUT, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(WD_OUTPUT, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA15

	gpio_set_pin_level(ACTIVATE_EBS,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(ACTIVATE_EBS, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(ACTIVATE_EBS, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA16

	gpio_set_pin_level(EN_ASSI_YELLOW,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(EN_ASSI_YELLOW, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(EN_ASSI_YELLOW, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA17

	gpio_set_pin_level(EN_ASSI_BLUE,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(EN_ASSI_BLUE, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(EN_ASSI_BLUE, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA18

	gpio_set_pin_level(EBS_FAILURE_LIGHT,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(EBS_FAILURE_LIGHT, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(EBS_FAILURE_LIGHT, GPIO_PIN_FUNCTION_OFF);

	ADC_0_init();

	EXTERNAL_IRQ_0_init();

	FLASH_0_init();

	delay_driver_init();

	TIMER_0_init();
	CAN_0_init();
}
